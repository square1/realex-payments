<?php

/**
 * This file is part of the Realex package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license    MIT License
 */

namespace Realex\Tests;

/**
 * @author Shane O'Grady <shane.ogrady@gmail.com>
 */
class TestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @return HttpAdapterInterface
     */
    protected function getMockAdapter($expects = null)
    {
        if (null === $expects) {
            $expects = $this->once();
        }

        $mock = $this->getMock('\Realex\HttpAdapter\HttpAdapterInterface');
        $mock
            ->expects($expects)
            ->method('postRequest')
            ->will($this->returnArgument(0));

        return $mock;
    }


    /**
     * Test checkMandatoryParamsAreSet when all params are set
     *
     * @return void
     */
    public function testCheckMandatoryParamsAreSet()
    {
        $params = array(
            'card'      =>  'xxxxxxxxxxxx1234',
            'expiry'    =>  '0714',
            'name'      =>  'Joe Bloggs'
        );
        $mandatoryParams = array(
            'card', 'expiry'
        );
        $this->assertTrue(\Realex\Realex::checkMandatoryParamsAreSet($params, $mandatoryParams));
    }


    /**
     * Test checkMandatoryParamsAreSet when a required param is missing
     *
     * @return void
     */
    public function testCheckMandatoryParamsAreSetMissingRequired()
    {
        $this->setExpectedException('\ErrorException');

        $params = array(
            'card'      =>  'xxxxxxxxxxxx1234',
            'name'      =>  'Joe Bloggs'
        );
        $mandatoryParams = array(
            'card', 'expiry'
        );

        \Realex\Realex::checkMandatoryParamsAreSet($params, $mandatoryParams);
    }


    /**
     * Test checkMandatoryParamsAreSet when no params are set
     *
     * @return void
     */
    public function testCheckMandatoryParamsAreSetMissingParams()
    {
        $this->setExpectedException('\ErrorException');

        $params = array();
        $mandatoryParams = array(
            'card', 'expiry'
        );

        \Realex\Realex::checkMandatoryParamsAreSet($params, $mandatoryParams);
    }


    /**
     * Test checkMandatoryParamsAreSet when no mandatory ones are set
     *
     * @return void
     */
    public function testCheckMandatoryParamsAreSetMissingMandatory()
    {
        $params = array(
            'card'      =>  'xxxxxxxxxxxx1234',
            'name'      =>  'Joe Bloggs'
        );
        $mandatoryParams = array();

        $this->assertTrue(\Realex\Realex::checkMandatoryParamsAreSet($params, $mandatoryParams));
    }

}
