<?php

/**
 * This file is part of the Realex package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license    MIT License
 */

namespace Realex;

use Realex\Request\VoidRequest;
use Realex\Request\DeleteCardRequest;
use Realex\Request\AddPayerRequest;
use Realex\Request\UpdatePayerRequest;
use Realex\Request\AddCardRequest;
use Realex\Request\UpdateCardRequest;
use Realex\Request\DoPaymentRequest;
use Realex\Response\VoidResponse;
use Realex\Response\AddPayerResponse;
use Realex\Response\AddCardResponse;
use Realex\Response\UpdateCardResponse;
use Realex\Response\UpdatePayerResponse;
use Realex\Response\DeleteCardResponse;
use Realex\Response\DoPaymentResponse;
use Realex\HttpAdapter\CurlHttpAdapter;
use Realex\HttpAdapter\BuzzHttpAdapter;

/**
 * @author Shane O'Grady <shane.ogrady@gmail.com>
 */
class Realex
{
    private static $instance = null;

    private static $endpoint;

    private static $userAgent;

    private static $hashAlgorithm;

    private static $merchantId;

    private static $secret;

    private static $adapter;

    private static $account;

    private static $settled = false;

    public static function getInstance()
    {
        if (!self::isInstantiated()) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function getEndpoint()
    {
        if (!self::isSettled()) {
            throw new \ErrorException('Realex engine not settled');
        }
        return self::$endpoint;
    }

    public static function getUserAgent()
    {
        if (!self::isSettled()) {
            throw new \ErrorException('Realex engine not settled');
        }
        return self::$userAgent;
    }

    public static function getHashAlgorithm()
    {
        if (!self::isSettled()) {
            throw new \ErrorException('Realex engine not settled');
        }
        return self::$hashAlgorithm;
    }

    public static function getMerchantId()
    {
        if (!self::isSettled()) {
            throw new \ErrorException('Realex engine not settled');
        }
        return self::$merchantId;
    }

    public static function getSecret()
    {
        if (!self::isSettled()) {
            throw new \ErrorException('Realex engine not settled');
        }
        return self::$secret;
    }

    public static function getAccount()
    {
        if (!self::isSettled()) {
            throw new \ErrorException('Realex engine not settled');
        }
        return self::$account;
    }

    public static function getAdapter()
    {
        if (!self::isSettled()) {
            throw new \ErrorException('Realex engine not settled');
        }
        return self::$adapter;
    }
    public static function createAdapter()
    {
        switch(self::getAdapter()){
            case 'curl':
                return new CurlHttpAdapter;
                break;
            case 'buzz':
                return new BuzzHttpAdapter;
                break;
            default:
                throw new \ErrorException('Invalid adapter');
        }
    }

    public static function setProperty($property, $value)
    {
        if (!self::isInstantiated()) {
            throw new \ErrorException('Realex engine not instantiated');
        }
        self::${$property} = $value;
    }

    public static function load($params = array())
    {
        self::getInstance();

        if (!is_array($params)) {
            throw new \ErrorException('Bad params when loading Realex');
        }
        $params = array_filter($params);
        $params = array_intersect_key(
            $params,
            array_flip(
                array('endpoint', 'userAgent','hashAlgorithm','merchantId','secret', 'adapter', 'account')
            )
        );

        //mandatory fields
        if (!array_key_exists('merchantId', $params)) {
            throw new \ErrorException('Bad params when loading Realex: merchantId is mandatory');
        }

        if (!array_key_exists('secret', $params)) {
            throw new \ErrorException('Bad params when loading Realex: secret is mandatory');
        }

        //set to default fields
        if (!array_key_exists('endpoint', $params)) {
            $params['endpoint'] = "https://epage.payandshop.com/epage-remote.cgi";
        }

        if (!array_key_exists('userAgent', $params)) {
            $params['userAgent'] = "Realex PHP Library";
        }

        if (!array_key_exists('hashAlgorithm', $params)) {
            $params['hashAlgorithm'] = "sha1";
        } else {
            if (!in_array($params['hashAlgorithm'], array('md5', 'sha1'))) {
                throw new \ErrorException('Bad params when loading Realex: hashAlgorithm must be md5 or sha1');
            }
        }

        if (!array_key_exists('adapter', $params)) {
            $params['adapter'] = "curl";
        } else {
            if (!in_array($params['adapter'], array('curl', 'buzz'))) {
                throw new \ErrorException('Bad params when loading Realex: adapter must be curl or buzz');
            }
        }

        if (!array_key_exists('account', $params)) {
            $params['account'] = "internet";
        } else {
            if (!in_array($params['account'], array('internet', 'internettest'))) {
                throw new \ErrorException('Bad params when loading Realex: account must be internet or internettest');
            }
        }

        self::$settled = true;

        foreach ($params as $param => $value) {
            self::setProperty($param, $value);
        }
    }

    public static function isInstantiated()
    {
        return (!is_null(self::$instance));
    }

    public static function isSettled()
    {
        return (self::isInstantiated() && self::$settled);
    }

    public static function doVoidRequest($params = array())
    {
        $mandatoryParams = array('order_id', 'pas_ref', 'auth_code', 'timestamp');
        self::checkMandatoryParamsAreSet($params, $mandatoryParams);

        $request = new VoidRequest(self::createAdapter());
        $request->setMerchantId(self::getMerchantId())
            ->setSecret(self::getSecret())
            ->setAccount(self::getAccount())
            ->setHashAlgorithm(self::getHashAlgorithm())
            ->setTimestamp($params['timestamp'])
            ->setOrderId($params['order_id'])
            ->setPasRef($params['pas_ref'])
            ->setAuthCode($params['auth_code'])
            ->setCustomerNumber(isset($params['customer_number']) ? $params['customer_number'] : '');

        return new VoidResponse(self::getSecret(), $request->execute());
    }

    public static function doAddPayerRequest($params = array())
    {
        $mandatoryParams = array('order_id', 'firstname', 'surname', 'country', 'email', 'payer_ref');
        self::checkMandatoryParamsAreSet($params, $mandatoryParams);

        $request = new AddPayerRequest(self::createAdapter());
            $request->setMerchantId(self::getMerchantId())
            ->setSecret(self::getSecret())
            ->setHashAlgorithm(self::getHashAlgorithm())
            ->setAccount(self::getAccount())
            ->setTimestamp()
            ->setOrderId($params['order_id'])
            ->setTitle(isset($params['title']) ? $params['title'] : '')
            ->setFirstName($params['firstname'])
            ->setSurname($params['surname'])
            ->setStreet(isset($params['street']) ? $params['street'] : '')
            ->setArea(isset($params['area']) ? $params['area'] : '')
            ->setCity(isset($params['city']) ? $params['city'] : '')
            ->setCounty(isset($params['county']) ? $params['county'] : '')
            ->setCountry($params['country'])
            ->setCountryCode(isset($params['country_code']) ? $params['country_code'] : '')
            ->setPhone(isset($params['phone']) ? $params['phone'] : '')
            ->setEmail($params['email'])
            ->setPayerRef($params['payer_ref'])
            ->setCustomerNumber(isset($params['customer_number']) ? $params['customer_number'] : '');

        return new AddPayerResponse(self::getSecret(), $request->execute());
    }

    public static function doUpdatePayerRequest($params = array())
    {
        $mandatoryParams = array('order_id', 'firstname', 'surname', 'country', 'email', 'payer_ref');
        self::checkMandatoryParamsAreSet($params, $mandatoryParams);

        $request = new UpdatePayerRequest(self::createAdapter());
            $request->setMerchantId(self::getMerchantId())
            ->setSecret(self::getSecret())
            ->setHashAlgorithm(self::getHashAlgorithm())
            ->setTimestamp()
            ->setAccount(self::getAccount())
            ->setOrderId($params['order_id'])
            ->setTitle(isset($params['title']) ? $params['title'] : '')
            ->setFirstName($params['firstname'])
            ->setSurname($params['surname'])
            ->setStreet(isset($params['street']) ? $params['street'] : '')
            ->setArea(isset($params['area']) ? $params['area'] : '')
            ->setCity(isset($params['city']) ? $params['city'] : '')
            ->setCounty(isset($params['county']) ? $params['county'] : '')
            ->setCountry($params['country'])
            ->setCountryCode(isset($params['country_code']) ? $params['country_code'] : '')
            ->setPhone(isset($params['phone']) ? $params['phone'] : '')
            ->setEmail($params['email'])
            ->setPayerRef($params['payer_ref'])
            ->setCustomerNumber(isset($params['customer_number']) ? $params['customer_number'] : '');

        return new UpdatePayerResponse(self::getSecret(), $request->execute());
    }

    public static function doAddCardRequest($params = array())
    {
        $mandatoryParams = array('order_id', 'card_ref', 'payer_ref', 'card_number', 'card_exp',
            'card_holder', 'card_type');
        self::checkMandatoryParamsAreSet($params, $mandatoryParams);

        $request = new AddCardRequest(self::createAdapter());
            $request->setMerchantId(self::getMerchantId())
            ->setSecret(self::getSecret())
            ->setHashAlgorithm(self::getHashAlgorithm())
            ->setTimestamp()
            ->setAccount(self::getAccount())
            ->setOrderId($params['order_id'])
            ->setPayerRef($params['payer_ref'])
            ->setCardRef($params['card_ref'])
            ->setCardNumber($params['card_number'])
            ->setCardExp($params['card_exp'])
            ->setCardHolder($params['card_holder'])
            ->setCardType($params['card_type'])
            ->setCustomerNumber(isset($params['customer_number']) ? $params['customer_number'] : '');

        return new AddCardResponse(self::getSecret(), $request->execute());
    }

    public static function doUpdateCardRequest($params = array())
    {
        $mandatoryParams = array('card_ref', 'payer_ref', 'card_exp', 'card_holder', 'card_type');
        self::checkMandatoryParamsAreSet($params, $mandatoryParams);

        $request = new UpdateCardRequest(self::createAdapter());
            $request->setMerchantId(self::getMerchantId())
            ->setSecret(self::getSecret())
            ->setAccount(self::getAccount())
            ->setHashAlgorithm(self::getHashAlgorithm())
            ->setTimestamp()
            ->setPayerRef($params['payer_ref'])
            ->setCardRef($params['card_ref'])
            ->setCardExp($params['card_exp'])
            ->setCardHolder($params['card_holder'])
            ->setCardType($params['card_type'])
            ->setCustomerNumber(isset($params['customer_number']) ? $params['customer_number'] : '');

        return new UpdateCardResponse(self::getSecret(), $request->execute());
    }

    public static function doDeleteCardRequest($params = array())
    {
        $mandatoryParams = array('card_ref', 'payer_ref');
        self::checkMandatoryParamsAreSet($params, $mandatoryParams);

        $request = new DeleteCardRequest(self::createAdapter());
            $request->setMerchantId(self::getMerchantId())
            ->setSecret(self::getSecret())
            ->setAccount(self::getAccount())
            ->setHashAlgorithm(self::getHashAlgorithm())
            ->setTimestamp()
            ->setPayerRef($params['payer_ref'])
            ->setCardRef($params['card_ref'])
            ->setCustomerNumber(isset($params['customer_number']) ? $params['customer_number'] : '');

        return new DeleteCardResponse(self::getSecret(), $request->execute());
    }

    public static function doPaymentRequest($params = array())
    {
        $mandatoryParams = array('card_ref', 'payer_ref', 'amount', 'order_id');
        self::checkMandatoryParamsAreSet($params, $mandatoryParams);

        $request = new DoPaymentRequest(self::createAdapter());
            $request->setMerchantId(self::getMerchantId())
            ->setSecret(self::getSecret())
            ->setAccount(self::getAccount())
            ->setHashAlgorithm(self::getHashAlgorithm())
            ->setTimestamp()
            ->setPayerRef($params['payer_ref'])
            ->setCardRef($params['card_ref'])
            ->setAmount($params['amount'])
            ->setOrderId($params['order_id'])
            ->setCustomerNumber(isset($params['customer_number']) ? $params['customer_number'] : '')
            ->setAutoSettle(isset($params['auto_settle']) ? $params['auto_settle'] : '1');

        return new DoPaymentResponse(self::getSecret(), $request->execute());
    }


    /**
     * Check whether all mandatory params are set
     *
     * @param array $params          Given parameters
     * @param array $mandatoryParams Mandatory parameters
     *
     * @throws \ErrorException
     *
     * @return bool
     */
    public static function checkMandatoryParamsAreSet($params = array(), $mandatoryParams = array())
    {
        // Strip any empty values
        $params = array_filter($params);
        foreach ($mandatoryParams as $mandatoryParam) {
            if (!array_key_exists($mandatoryParam, $params)) {
                throw new \ErrorException("Mandatory param $mandatoryParam not set");
            }
        }

        // If we're here without throwing an exception, all is well
        return true;
    }
}
