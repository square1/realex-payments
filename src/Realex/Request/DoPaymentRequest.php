<?php

/**
 * This file is part of the Realex package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license    MIT License
 */

namespace Realex\Request;

/**
 * @author Shane O'Grady <shane.ogrady@gmail.com>
 */
class DoPaymentRequest extends AbstractRequest implements RequestInterface
{
    /**
     * @var string
     */
    protected $order_id = null;

    /**
     * @var string
     */
    protected $payer_ref = null;

    /**
     * @var string
     */
    protected $card_ref = null;

    /**
     * @var float
     */
    protected $amount = null;

    /**
     * @var string
     */
    protected $customer_number = null;


    /**
     * @var integer
     */
    protected $auto_settle = 1;

    /**
     * {@inheritDoc}
     */
    public function getXml()
    {
        $this->setHash();

        $hash = "<{$this->hash_algorithm}hash>{$this->getHash()}</{$this->hash_algorithm}hash>";

        $xml = <<<XML
<request type='{$this->getName()}' timestamp='{$this->getTimestamp()}'>
    <merchantid>{$this->getMerchantId()}</merchantid>
    <account>{$this->getAccount()}</account>
    <orderid>{$this->getOrderId()}</orderid>
    <amount currency='EUR'>{$this->getAmount()}</amount>
    <payerref>{$this->getPayerRef()}</payerref>
    <paymentmethod>{$this->getCardRef()}</paymentmethod>
    <autosettle flag='{$this->getAutoSettle()}' />
    <tssinfo>
        <custnum>{$this->getCustomerNumber()}</custnum>
    </tssinfo>
    {$hash}
</request>
XML;
        return $xml;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return "receipt-in";
    }

        /**
     * {@inheritDoc}
     */
    protected function getHashFields()
    {
        return implode(
            ".",
            array(
                $this->getTimestamp(),
                $this->getMerchantId(),
                $this->getOrderId(),
                $this->getAmount(),
                "EUR",
                $this->getPayerRef()
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function validate()
    {
        // @todo: Validation logic
        return true;
    }

    /**
     * Returns the payer ref
     *
     * @return string
     */
    public function getPayerRef()
    {
        return $this->payer_ref;
    }

    /**
     * Sets the payer ref to be used.
     *
     * @param string $payer_ref
     *
     * @return AddCardRequest
     */
    public function setPayerRef($payer_ref)
    {
        $this->payer_ref = $payer_ref;

        return $this;
    }

    /**
     * Returns the card ref
     *
     * @return string
     */
    public function getCardRef()
    {
        return $this->card_ref;
    }

    /**
     * Sets the card ref to be used.
     *
     * @param string $card_ref
     *
     * @return AddCardRequest
     */
    public function setCardRef($card_ref)
    {
        $this->card_ref = $card_ref;

        return $this;
    }

    /**
     * Returns the amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Sets the amount to be used.
     *
     * @param string $amount
     *
     * @return DoPaymentRequest
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Returns the order ID
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Sets the order ID to be used.
     *
     * @param string $order_id
     *
     * @return AddPayerRequest
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;

        return $this;
    }

    /**
     * Returns the customer number
     *
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customer_number;
    }

    /**
     * Sets the customer number to be used. The field is a bit of a misnomer
     * as it does not have to be strictly numeric.
     *
     * @param string $customer_number Customer number
     *
     * @return AddCardRequest
     */
    public function setCustomerNumber($customer_number)
    {
        $this->customer_number = $customer_number;

        return $this;
    }


    /**
     * Sets the auto settle flag on the transaction.
     *
     * @param int $autoSettle Desired auto-settle value
     *
     * @return AddCardRequest
     */
    public function setAutoSettle($autoSettle)
    {
        // Only over-write it if it's a valid auto-settle value
        if (in_array($autoSettle, array(0, 1))) {
            $this->auto_settle = $autoSettle;
        }

        return $this;
    }


    /**
     * Get the auto-settle value
     *
     * @return int Auto settle flag
     */
    public function getAutoSettle()
    {
        return $this->auto_settle;
    }
}
