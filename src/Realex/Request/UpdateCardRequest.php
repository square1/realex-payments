<?php

/**
 * This file is part of the Realex package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license    MIT License
 */

namespace Realex\Request;

/**
 * @author Shane O'Grady <shane.ogrady@gmail.com>
 */
class UpdateCardRequest extends AbstractRequest implements RequestInterface
{
    /**
     * @var string
     */
    protected $order_id = null;

    /**
     * @var string
     */
    protected $payer_ref = null;

    /**
     * @var string
     */
    protected $card_ref = null;

    /**
     * @var string
     */
    protected $card_exp = null;

    /**
     * @var string
     */
    protected $card_holder = null;

    /**
     * @var string
     */
    protected $card_type = null;

    /**
     * @var string
     */
    protected $card_number = null;

    /**
     * @var string
     */
    protected $customer_number = null;

    /**
     * {@inheritDoc}
     */
    public function getXml()
    {
        $this->setHash();

        $hash = "<{$this->hash_algorithm}hash>{$this->getHash()}</{$this->hash_algorithm}hash>";

        $xml = <<<XML
<request type='{$this->getName()}' timestamp='{$this->getTimestamp()}'>
    <merchantid>{$this->getMerchantId()}</merchantid>
    <account>{$this->getAccount()}</account>
    <card>
        <ref>{$this->getCardRef()}</ref>
        <payerref>{$this->getPayerRef()}</payerref>
        <expdate>{$this->getCardExp()}</expdate>
        <chname>{$this->getCardHolder()}</chname>
        <type>{$this->getCardType()}</type> 
    </card>
    <tssinfo>
        <custnum>{$this->getCustomerNumber()}</custnum>
    </tssinfo>
    {$hash}
</request>
XML;
        return $xml;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return "card-update-card";
    }

        /**
     * {@inheritDoc}
     */
    protected function getHashFields()
    {
        return implode(
            ".",
            array(
                $this->getTimestamp(),
                $this->getMerchantId(),
                $this->getPayerRef(),
                $this->getCardRef(),
                $this->getCardExp(),
                ""
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function validate()
    {
        // @todo: Validation logic
        return true;
    }

    /**
     * Returns the payer ref
     *
     * @return string
     */
    public function getPayerRef()
    {
        return $this->payer_ref;
    }

    /**
     * Sets the payer ref to be used.
     *
     * @param string $payer_ref
     *
     * @return AddCardRequest
     */
    public function setPayerRef($payer_ref)
    {
        $this->payer_ref = $payer_ref;

        return $this;
    }

    /**
     * Returns the card ref
     *
     * @return string
     */
    public function getCardRef()
    {
        return $this->card_ref;
    }

    /**
     * Sets the card ref to be used.
     *
     * @param string $card_ref
     *
     * @return AddCardRequest
     */
    public function setCardRef($card_ref)
    {
        $this->card_ref = $card_ref;

        return $this;
    }

    /**
     * Returns the expiry date
     *
     * @return string
     */
    public function getCardExp()
    {
        return $this->card_exp;
    }

    /**
     * Sets the expiry date to be used.
     *
     * @param string $card_exp
     *
     * @return AddCardRequest
     */
    public function setCardExp($card_exp)
    {
        $this->card_exp = $card_exp;

        return $this;
    }

    /**
     * Returns the holder name
     *
     * @return string
     */
    public function getCardHolder()
    {
        return $this->card_holder;
    }

    /**
     * Sets the card holder name to be used.
     *
     * @param string $card_holder
     *
     * @return AddCardRequest
     */
    public function setCardHolder($card_holder)
    {
        $this->card_holder = $card_holder;

        return $this;
    }

    /**
     * Returns the card type
     *
     * @return string
     */
    public function getCardType()
    {
        return $this->card_type;
    }

    /**
     * Sets the card type to be used.
     *
     * @param string $card_type
     *
     * @return AddCardRequest
     */
    public function setCardType($card_type)
    {
        $this->card_type = $card_type;

        return $this;
    }

    /**
     * Returns the customer number
     *
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customer_number;
    }

    /**
     * Sets the customer number to be used. The field is a bit of a misnomer
     * as it does not have to be strictly numeric.
     *
     * @param string $customer_number Customer number
     *
     * @return AddCardRequest
     */
    public function setCustomerNumber($customer_number)
    {
        $this->customer_number = $customer_number;

        return $this;
    }
}
