<?php

/**
 * This file is part of the Realex package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license    MIT License
 */

namespace Realex\Request;

/**
 * @author Shane O'Grady <shane.ogrady@gmail.com>
 */
class AddPayerRequest extends AbstractRequest implements RequestInterface
{
    /**
     * @var string
     */
    protected $order_id = null;

    /**
     * @var string
     */
    protected $payer_ref = null;

    /**
     * @var string
     */
    protected $title = null;

    /**
     * @var string
     */
    protected $firstname = null;

    /**
     * @var string
     */
    protected $surname = null;

    /**
     * @var string
     */
    protected $street = null;

    /**
     * @var string
     */
    protected $area = null;

    /**
     * @var string
     */
    protected $city = null;

    /**
     * @var string
     */
    protected $county = null;

    /**
     * @var string
     */
    protected $country_code = null;

    /**
     * @var string
     */
    protected $country = null;

    /**
     * @var string
     */
    protected $phone = null;

    /**
     * @var string
     */
    protected $email = null;

    /**
     * @var string
     */
    protected $customer_number = null;

    /**
     * {@inheritDoc}
     */
    public function getXml()
    {
        $this->setHash();

        $hash = "<{$this->hash_algorithm}hash>{$this->getHash()}</{$this->hash_algorithm}hash>";

        $xml = <<<XML
<request type='{$this->getName()}' timestamp='{$this->getTimestamp()}'>
    <merchantid>{$this->getMerchantId()}</merchantid>
    <orderid>{$this->getOrderId()}</orderid>
    <account>{$this->getAccount()}</account>
    <payer type='Business' ref='{$this->getPayerRef()}'>
    	<title>{$this->getTitle()}</title> 
    	<firstname>{$this->getFirstName()}</firstname>
		<surname>{$this->getSurname()}</surname> 
		<address>
			<line1>{$this->getStreet()}</line1>
            <line2>{$this->getArea()}</line2>
			<city>{$this->getCity()}</city>
			<county>{$this->getCounty()}</county>
			<country code='{$this->getCountryCode()}'>{$this->getCountry()}</country> 
		</address>
		<phonenumbers>
			<home>{$this->getPhone()}</home>
		</phonenumbers>
		<email>{$this->getEmail()}</email> 
    </payer>
    <tssinfo>
        <custnum>{$this->getCustomerNumber()}</custnum>
    </tssinfo>
    {$hash}
</request>
XML;

        return $xml;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return "payer-new";
    }

        /**
     * {@inheritDoc}
     */
    protected function getHashFields()
    {
        return implode(
            ".",
            array(
                $this->getTimestamp(),
                $this->getMerchantId(),
                $this->getOrderId(),
                "",
                "",
                $this->getPayerRef()
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function validate()
    {
        // @todo: Validation logic
        return true;
    }

    /**
     * Returns the order ID
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Sets the order ID to be used.
     *
     * @param string $order_id
     *
     * @return AddPayerRequest
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;

        return $this;
    }

    /**
     * Returns the payer ref
     *
     * @return string
     */
    public function getPayerRef()
    {
        return $this->payer_ref;
    }

    /**
     * Sets the payer ref to be used.
     *
     * @param string $payer_ref
     *
     * @return AddPayerRequest
     */
    public function setPayerRef($payer_ref)
    {
        $this->payer_ref = $payer_ref;

        return $this;
    }

    /**
     * Returns the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title to be used.
     *
     * @param string $title
     *
     * @return AddPayerRequest
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Returns the first name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstname;
    }

    /**
     * Sets the first name to be used.
     *
     * @param string $firstname
     *
     * @return AddPayerRequest
     */
    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Returns the surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Sets the surname to be used.
     *
     * @param string $surname
     *
     * @return AddPayerRequest
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Returns the street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street to be used.
     *
     * @param string $street
     *
     * @return AddPayerRequest
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Returns the area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Sets the area to be used.
     *
     * @param string $area
     *
     * @return AddPayerRequest
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Returns the city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city to be used.
     *
     * @param string $city
     *
     * @return AddPayerRequest
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Returns the county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Sets the county to be used.
     *
     * @param string $county
     *
     * @return AddPayerRequest
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Returns the country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country to be used.
     *
     * @param string $country
     *
     * @return AddPayerRequest
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Returns the country code
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * Sets the country code to be used.
     *
     * @param string $country_code
     *
     * @return AddPayerRequest
     */
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;

        return $this;
    }

   	/**
     * Returns the phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone to be used.
     *
     * @param string $phone
     *
     * @return AddPayerRequest
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Returns the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email to be used.
     *
     * @param string $email
     *
     * @return AddPayerRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Returns the customer number
     *
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customer_number;
    }

    /**
     * Sets the customer number to be used. The field is a bit of a misnomer
     * as it does not have to be strictly numeric.
     *
     * @param string $customer_number Customer number
     *
     * @return AddCardRequest
     */
    public function setCustomerNumber($customer_number)
    {
        $this->customer_number = $customer_number;

        return $this;
    }
}
